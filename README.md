# y9-workspace
Ubuntu desktop with python for kasm workspaces

## Name
Choose a self-explaining name for your project.

## Description
This is a kasm workspace for learning python.

## Support
lem@rathkeale.school.nz

## See More Info
Open-Source https://github.com/kasmtech
<br>https://www.kasmweb.com/
