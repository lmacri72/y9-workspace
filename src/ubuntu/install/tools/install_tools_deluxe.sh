#!/usr/bin/env bash
set -ex
apt-get update
apt-get install -y vlc git tmux
apt-get install -y mousepad unzip gnome-keyring gdebi-core kmod
